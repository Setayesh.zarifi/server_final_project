<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');




Route::group(['middleware' => ['jwt.verify']], function () {
    Route::post('/logout', 'AuthController@logout');
    Route::get('user/me', 'AuthController@currentUser');
    Route::post('/createCompany', 'CompanyController@createCompany');   
    Route::get('/userCompanies', 'CompanyController@userCompanies');
    Route::get('/allCompanies' , 'CompanyController@allCompanies');
    Route::post('/addEmployee' , 'EmployeeController@addEmployee');
    Route::post('/getEmployee' , 'EmployeeController@getEmployee');      
    Route::post('/updateLocation' , 'LocationController@updateLocation');
    Route::post('/editEmployeeDetails' , 'EmployeeController@editEmployeeDetails');  
    Route::post('/GetEmployeesLocations' , 'LocationController@GetEmployeesLocations');   
    Route::post('/changePassword', 'AuthController@changePassword');    
    Route::post('/editCompanyDetails', 'CompanyController@editCompanyDetails');   
    Route::post('/getEmployeeInfo' , 'EmployeeController@getEmployeeInfo'); 
});
