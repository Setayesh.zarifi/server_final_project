<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
     public $timestamps = false;

     protected $fillable = [
         'licensePlate','userId','company_id'
    ];

    public function isEmployedIn()
    {
        return $this->belongsTo('App\Company','id');
    }

    public function getLocation(){
        return $this->hasMany('App\Location','id','employee_id');
    }
}
