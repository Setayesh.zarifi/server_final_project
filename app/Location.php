<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    public $timestamps = false;

        protected $fillable = [
         'longitude','latitude' , 'employee_id'
    ];
    public function getEmployee()
    {
        return $this->belongsTo('App\Employee','id');
    }


}
