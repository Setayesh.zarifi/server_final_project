<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Auth;
use App\Employee;

class CompanyController extends Controller
{
    //
    public function createCompany(Request $request)
    {
        $company = Company::create([
            'name'    => $request->input('name'),
            'userId' => Auth::user()->id,
         ]);

         $company->save();
         return response()->json([
             'message' => 'company created',
         ],200)  ;    
    }

    public function allCompanies(Request $request){
        $companies = \App\Company::all();
        return response()->json([
            'name' => $companies ->name ,
            'userId' => $companies ->userId            
        ],200)  ;    
    }

    public function userCompanies(Request $request){
        $user = Auth::user() ;
        $companies = Company::where("userId",$user->id)->get();
        foreach ($companies as $com) {
            $com["employeeNumber"] = Employee::where("company_id",$com->id)->count();
        }
        return response()->json([
            'companies' =>  $companies
        ],200)  ;   

    }


    

      public function editCompanyDetails(Request $request){
       
         Company::where('id',$request->input('company_id'))->update(['name' => $request->input('companyName') ]);
        return response()->json([
            'message' =>  "Company Details Updated"
        ],200)  ;   

    }
    

}
