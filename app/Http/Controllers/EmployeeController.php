<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Employee;
use App\User;
use Auth;


class EmployeeController extends Controller
{
    //

    public function addEmployee(Request $request){
        $user = User::create([    
             'phoneNum'    => $request->input('phoneNum'),
             'password' => substr($request->input('phoneNum'),-4),
             'name'    => $request->input('name'),
             'familyName' => $request->input('familyName'),
         ]);
        $employee = employee::create([
            
            'licensePlate' => $request->input('licensePlate'),
            'userId' =>  $user-> id,
            'company_id' => $request->input('companyId'),
         ]);

         return response()->json([
             'message' => 'Employee Added',
         ],200);    
    }

    public function getEmployee(Request $request){
        $employees = Employee::where('company_id',$request->input('company_id'))->get();
        foreach ($employees as $emp) {
           $user = User::where('id',$emp->userId)->get();
           $emp["name"] = $user[0]->name;
           $emp["familyName"] = $user[0]->familyName;
           $emp["userId"] = $user[0]->id;
           $emp["phoneNum"] = $user[0]->phoneNum;
           
        }
        return response()->json([
            'employees' =>  $employees
        ],200)  ;      
    }


    public function editEmployeeDetails(Request $request){
      
       $licensePlate = $request->input('licensePlate');
       $employeeId = $request->input('employeeId');
       $phoneNum = $request->input('phoneNum');
       $name = $request->input('name');
       $familyName = $request->input('familyName');
       $userId = $request->input('userId');

        Employee::where('id',$employeeId)->update(['licensePlate' => $licensePlate ]);
        User::where('id',$userId)->update(['familyName' => $familyName ]);
        User::where('id',$userId)->update(['name' => $name ]);
        User::where('id',$userId)->update(['phoneNum' => $phoneNum ]);
        User::where('id',$userId)->update(['password' => bcrypt(substr($phoneNum,-4))]);

        return response()->json([
            'message' => 'Employee Details Updated',
        ],200)  ;      
    }


        public function getEmployeeInfo(Request $request){
             $user = Auth::user();
             $employee = Employee::where('userId',$user->id)->first();
            return response()->json([
                'phoneNum' =>  $user ->phoneNum,
                'licensePlate'=>$employee->licensePlate
            ],200)  ;      
        }



}

