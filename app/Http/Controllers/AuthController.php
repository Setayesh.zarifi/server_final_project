<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {
        
        $user = User::create([    
             'phoneNum'    => $request->input('phoneNum'),
             'password' => $request->input('password'),
             'name'    => $request->input('name'),
             'familyName' => $request->input('familyName'),
             
         ]);

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login()
    {
        $credentials = request(['phoneNum', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }

    public function currentUser(Request $request){
        
        $user = User::find(auth()->user()->id);
        return response()->json([
            'user'   => $user,
        ]);
    }
    

     public function changePassword(Request $request){
        
        $user = User::find(auth()->user()) -> first();
        if(password_verify($request->input('currentPass'),$user -> password )){
             User::where('id',$user -> id)->update(['password' => bcrypt($request->input('newPass'))]);
             return response()->json([
                    'message'   => "Password Updated",
            ]);
        }

        else{
             return response()->json([
                    'message'   => "Current Password is wrong!",
        ]);
        }
        
    }

    
}
