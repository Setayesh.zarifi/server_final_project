<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Employee;
use App\User;
use App\Location;
use Auth;


class LocationController extends Controller
{
    //
        public function updateLocation(Request $request){
        //      return response()->json([
        //      'requset' => $request->input('longitude')
        //  ],200); 
                
             $employee = Employee::where('userId', '=',Auth::user()->id )->first();
             if($employee == null){
                 return response()->json([
                    'message' => 'Sorry you are not an employee!',
                ],200); 
             }
    
             $location = Location::where('employee_id', '=',$employee->id )->first();
                
           
            if ($location === null) {
            // user doesn't exist
               
                $loc = Location::create([
                'longitude'    => $request->input('longitude'),
                'latitude' => $request->input('latitude'),
                'employee_id'    => $employee ->id
                ]);
 
            }
            else{
                 Location::where('employee_id',$employee-> id)->update(['longitude' => $request->input('longitude') ]);
                 Location::where('employee_id',$employee-> id)->update(['latitude' => $request->input('latitude') ]);
            }

         return response()->json([
             'message' => 'Location Updated',
         ],200);    
    }

    public function GetEmployeesLocations(Request $request){
         $employees = Employee::where('company_id',$request->input('company_id'))->get();
         $userLocs = [];
        foreach ($employees as $emp) {
           $userLoc["user"] = User::where('id',$emp->userId)->first();
           $userLoc["location"] = Location::where('employee_id',$emp-> id)-> first();
           array_push($userLocs,$userLoc );
           
        }
        return response()->json([
            'employees' =>  $userLocs
        ],200)  ;   
     
    }
    

}
