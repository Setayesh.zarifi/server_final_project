<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

   public $timestamps = false;
    
    protected $fillable = [
        'name', 'userId',
    ];

// protected $hidden = [
//      'updated_at',
//      "created_at"
//     ];


    public function employees()
    {
        return $this->hasMany(Employee::class,'id','company_id');
    }
}
